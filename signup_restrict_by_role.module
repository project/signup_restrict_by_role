<?php

/**
 * @file
 * General functions for the signup_restrict_by_role module.
 */

/**
 * Implementation of hook_form_FORM_ID_alter().
 */
function signup_restrict_by_role_form_signup_settings_form_alter(&$form, $form_state) {
  $form['signup_restrict_by_role'] = array(
    '#type' => 'fieldset',
    '#title' => t('Signup Restrict by Role'),
    '#description' => t('Settings for restricting signups to users with specific roles.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['signup_restrict_by_role']['signup_restrict_by_role_signup_suppress'] = array(
    '#type' => 'checkbox',
    '#title' => t('Repress signup output to restricted roles'),
    '#description' => t("When enabled signup output will be restricted to specific roles. Leave this enabled unless you want another module to restrict signups using a different method."),
    '#default_value' => variable_get('signup_restrict_by_role_signup_suppress', 1),
  );
  $form['signup_restrict_by_role']['signup_restrict_by_role_admin_all_bypass'] = array(
    '#type' => 'checkbox',
    '#title' => t("Allow users with 'administer all signups' permission to skip role restriction"),
    '#default_value' => variable_get('signup_restrict_by_role_admin_all_bypass', 1),
  );
  $form['signup_restrict_by_role']['signup_restrict_by_role_admin_own_bypass'] = array(
    '#type' => 'checkbox',
    '#title' => t("Allow the node author with 'administer signups for own content' permission to skip role restriction"),
    '#default_value' => variable_get('signup_restrict_by_role_admin_own_bypass', 1),
  );
}

/**
 * Implementation of hook_form_alter().
 */
function signup_restrict_by_role_form_alter(&$form, &$form_state, $form_id) {
  // Make sure we're on a node form.
  $node = $form['#node'];
  $is_node_form = $node && $form_id == "{$node->type}_node_form";

  // Check that signup settings should appear on this node.
  $signup_states = array('enabled_on', 'allowed_off');
  $is_signup_type = $node && in_array(variable_get("signup_node_default_state_{$node->type}", 'disabled'), $signup_states);

  if ($is_node_form && $is_signup_type) {
    // Alter the node form by reference.
    signup_restrict_by_role_node_settings_form($form, $form_state);
  }
}

/**
 * Implementation of hook_form_FORM_ID_alter().
 */
function signup_restrict_by_role_form_node_type_form_alter(&$form, &$form_state) {
  // Alter the node type configuration form by reference.
  signup_restrict_by_role_node_settings_form($form, $form_state);
}

/**
 * Returns the form for per-type & per-node signup restrict by roles settings.
 *
 * This is shared by each node type page and the node edit page.
 *
 * @see signup_restrict_by_role_form_alter()
 * @see signup_restrict_by_role_form_node_type_form_alter()
 */
function signup_restrict_by_role_node_settings_form(&$form, &$form_state) {
  // Get the node if we're on TYPE_node_form.
  $node = $form['#node'];

  // Build the options list.
  $roles = user_roles();
  foreach ($roles as $key => $value) {
    $roles_options[$key] = $value;
  }

  // Build the default value list.
  $default_options = array();
  // Existing node data is loaded by signup_restrict_by_role_nodeapi().
  if ($node && !empty($node->signup_restrict)) {
    $default_options = $node->signup_restrict;
  }
  // New nodes, existing nodes configured for signup that are signup disabled
  // and the node type form all load per-type defaults.
  else {
    $type = $form['form_id']['#value'] == 'node_type_form' ? $form['#node_type']->type : $node->type;
    $default_options = variable_get("signup_restrict_{$type}", array());
  }

  // Add the form element to toggle if signups are allowed.
  $form['signup']['signup_restrict'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Restrict signups to certain roles'),
    '#options' => $roles_options,
    '#default_value' => $default_options,
  );
}

/**
 * Implementation of hook_nodeapi().
 */
function signup_restrict_by_role_nodeapi(&$node, $op, $teaser = NULL, $page = NULL) {
  global $user;
  switch ($op) {
    case 'load':
      // Load settings for existing signup-enabled nodes.
      if ($node->nid && $node->signup) {
        return array('signup_restrict' => signup_restrict_by_role_load_settings($node));
      }
      break;
    case 'insert':
    case 'update':
      db_query("DELETE FROM {signup_restrict_by_role} WHERE nid = %d", $node->nid);
      if (!empty($node->signup_restrict)) {
        foreach ($node->signup_restrict as $rid => $value) {
          db_query("INSERT INTO {signup_restrict_by_role} (nid, rid, value) VALUES (%d, %d, %d)", $node->nid, $rid, $value);
        }
      }
      break;
  }
}

/**
 * Implementation of hook_signup_open().
 *
 * Insert defaults in the DB in cases when a node that never saved signup
 * restrict settings is re-opened for signups without using the node form. This
 * could happen if, for example, signup_restrict_by_role is configured for a
 * content type with an existing signup disabled node due to signup limit
 * having been reached - that node could be re-opened (automatically when
 * someone cancels a signup so limit is no longer reached, or the manually re-
 * opened by an administrator).
 */
function signup_restrict_by_role_signup_open($node) {
  $result = db_result(db_query("SELECT COUNT(rid) FROM {signup_restrict_by_role} WHERE nid = %d", $node->nid));
  if (!$result) {
    $defaults = variable_get("signup_restrict_{$node->type}", array());
    foreach ($defaults as $rid) {
      db_query("INSERT INTO {signup_restrict_by_role} (nid, rid, value) VALUES (%d, %d, %d)", $node->nid, $rid, $rid);
    }
  }
}

/**
 * Implementation of hook_signup_suppress().
 *
 * If user should be restricted by role, don't print any signup-related output.
 */
function signup_restrict_by_role_signup_suppress($node) {
  if (variable_get('signup_restrict_by_role_signup_suppress', 1)) {
    global $user;
    $check = signup_restrict_by_role_access_signup($node, $user);
    if (!$check['success']) {
      drupal_set_message($check['message'], 'status');
      return TRUE;
    }
  }
}

/**
 * Helper function to load restrict by role settings per node.
 *
 * @param $node
 *   The fully-loaded node object.
 *
 * @return
 * Keyed array of roles, with a value either matching the role or 0.
 */
function signup_restrict_by_role_load_settings($node) {
  // Load settings for signup-enabled nodes. This also includes existing
  // nodes that have not explicitly saved this setting.
  // @see signup_restrict_by_role_signup_open()
  $signup_restrict = array();
  $result = db_query("SELECT * FROM {signup_restrict_by_role} WHERE nid = %d", $node->nid);
  while ($row = db_fetch_object($result)) {
    $signup_restrict[$row->rid] = $row->value;
  }
  return $signup_restrict;
}

/**
 * Helper function to determine signup restriction access by role.
 *
 * @param $node
 *   The fully-loaded node object being viewed.
 * @param $user
 *   A user object.
 *
 * @return associative array containing:
 *   - success: TRUE if the user should not be restricted by role.
 *   - message: (optional) if success is FALSE, message explaining why.
 */
function signup_restrict_by_role_access_signup($node, $user) {
  // Load settings for the node.
  $signup_restrict_settings = signup_restrict_by_role_load_settings($node);

  // Check if the node is unrestricted.
  $unrestricted = !array_filter($signup_restrict_settings);

  // Check if user has allowed role.
  foreach ($user->roles as $rid => $role) {
    if (in_array($rid, $signup_restrict_settings)) {
      $has_allowed_role = TRUE;
    }
  }

  // Check if user has administrative access to the node's signups.
  $admin_all = user_access('administer all signups');
  $admin_all_bypass = $admin_all && variable_get('signup_restrict_by_role_admin_all_bypass', 1);
  $admin_own = user_access('administer signups for own content') && ($user->uid == $node->uid);
  $admin_own_bypass = $admin_own && variable_get('signup_restrict_by_role_admin_own_bypass', 1);

  if ($unrestricted || $has_allowed_role || $admin_all_bypass || $admin_own_bypass) {
    return array(
      'success' => TRUE,
    );
  }
  else {
    // Display a failure message to the user.
    $role_names = user_roles();
    foreach ($signup_restrict_settings as $rid => $value) {
      if ($rid == $value) {
        $allowed_roles[$rid] = $role_names[$rid];
      }
    }
    $access_message = t('This signup is restricted to users with the following roles: %allowed_roles.', array(
      '%allowed_roles' => implode(', ', $allowed_roles),
    ));
    return array(
      'success' => FALSE,
      'message' => $access_message,
    );
  }
}

Module installation
===================
Follow the standard drupal installation procedure and copy the module files to 
sites/xxx/modules/signup_restrict_by_role/

You should then be able to enable the Signup Restrict by Role module in the
admin interface.

Checkboxes then become available in the signup section of signup enabled nodes.

If any checkboxes are selected, then access to signing up to the node is
restricted to people with that role.
